﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http.Description;
using System.Web.Mvc;
using Decanat.Models;
using Microsoft.Ajax.Utilities;
using System.Web.Script.Serialization;

namespace Decanat.Controllers
{
	public class VedomDocController : Controller
	{
		private JournalEntities context = new JournalEntities();

		public ActionResult Index(int pVedCode)
		{
		    if (Session["UserName"] == null)
		    {
		        // Что-нибудь делаем для авторизации пользователя
                Session["UserName"] = "Undefined, host: " + Request.UserHostName + "address: " + Request.UserHostAddress;
		    }
		    else
		    {
		        
		    }

            //int pVedCode = 298154;

            var curVedDescription = new VedomDescription(pVedCode, false);

            // Определим преподавателя для ведомости
            //if (curVedDescription.VedType == "Зачет")
            //{
            //    if (curVedDescription.FIOLab == "-")
            //        ViewBag.Prepod = curVedDescription.FIOPrakt;
            //}
            //else 
            //{
            //    ViewBag.Prepod = curVedDescription.FIOLektor;
            //}

            return View(curVedDescription);
		}

        /// <summary>
        /// Контроллер, формирующий печатную форму определенного типа
        /// </summary>
        /// <param name="pVedCode">Код ведомости</param>
        /// <param name="pPrintType">0 - Печать заполненной ведомости, 1 - Печать пустого бланка ведомости, 2 - Печать бланка с просавленными допусками</param>
        /// <returns></returns>
        public ActionResult PrintVedom(int pVedCode, int pPrintType)
        {
            VedomDocument VedDescr = new VedomDocument(pVedCode, true);
            ViewBag.PrintType = pPrintType;
            return View(VedDescr);
        }

		[HttpGet]
		public JsonResult GetOcenkiForVedom(int pVedCode)
		{

			var dbResult = context.GetOcenkiCollectionForVedom(pVedCode, null);

			var ocenki = (from oc in dbResult select new
						  {
                              oc.оц_Код,
							  oc.вс_Код,
							  УчетВВед=oc.УчетВВед,
							  ФИО=oc.Фамилия + " " + oc.Имя + " " + oc.Отчество,
							  Зачетка=oc.Зачетка,
							  oc.РейтинговыйБалл,
							  oc.ДопускПоОплате,
                              oc.Итоговая_Оценка,
                              oc.Итог,
                              oc.Рейтинг_Оценка,
                              oc.ИнтИОц,
                              oc.ИнтИОцО,
                              oc.ИнтИтогО,
                              oc.МожетПравить
						  });

			return Json(ocenki, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetBegunkiForStudent(int pStudentId, int pVedCode)
		{
			var dbResult = context.GetBegunkiForStudent(pStudentId, pVedCode);

			var begunki = (from b in dbResult select new
			{
                	b.id,
	                b.id_ved,
	                b.id_stud,
	                b.date_create,
	                b.date_give,
	                b.date_expired,
	                b.date_lastupdate,
	                b.ocenka,
	                b.description,
	                b.user_last,
                    b.isclosed
			});

			return Json(begunki, JsonRequestBehavior.AllowGet);
		}

        [HttpPost]
        public JsonResult SaveAllOcenki(string OcenkiItems)
        {
            // Сообщение пользователю
            string ResMessage = "";
            int LocalRes = 0;

            try
            {
                List<ItemHelper> Ocenki = new JavaScriptSerializer().Deserialize<List<ItemHelper>>(OcenkiItems);
                                            
                int LocCurrentVedom = Convert.ToInt32(Ocenki[0].VedomCode);

                // Получаем список допустимых оценок для ведомости
                var TypesOfOcenki = context.GetOcenkiForVedom(LocCurrentVedom).ToArray();
                
                // Сохраняем оценки по одной
                int LocCurTypeOfOcenka,
                    LocOcenkaCode = -1,
                    LocUchet = 0;

                string LocUser = (Session["UserName"] != null) ? Session["UserName"].ToString() : "Undef";
                
                foreach (var helper in Ocenki)
                {
                    LocCurTypeOfOcenka = Convert.ToInt32(helper.OcenkaTypeCode);
                    int a = 0;
                    // Проверяем, чтобы оценка попала в нужный диапазон
                    if (TypesOfOcenki.Any(item => item.КодИАСУ == LocCurTypeOfOcenka))
                    {
                        if (helper.OcenkaCode == null)
                            LocOcenkaCode = -1;
                        else
                        {
                            LocOcenkaCode = Convert.ToInt32(helper.OcenkaCode);
                        }
                        if (helper.Uchet.ToUpper() == "TRUE")
                            LocUchet = 1;
                        else
                            LocUchet = 0;

                        // Только тогда сохраняем
                        LocalRes = context.SetOcenkaVedom(
                            LocCurrentVedom, 
                            Convert.ToInt32(helper.StudentCode), 
                            LocOcenkaCode, 
                            LocCurTypeOfOcenka,
                            LocUchet,
                            LocUser);

                        // Потом формируем заключение
                        if (LocalRes > 0)
                        {
                            ResMessage += "> Сохранено. ФИО: " + helper.StudentFIO
                                + "<br/>признак учета: <b>" + ((LocUchet == 1) ? "Учитывать" : "Не учитывать")
                                + "</b>, код оценки: <b>" + LocCurTypeOfOcenka.ToString() + "</b><br/>";
                        }
                    }
                    else
                    {
                        // Формируем сообщение об ошибке
                        ResMessage += "Не удалось сохранить: " + helper.StudentFIO + " - недопустимый код оценки: " + LocCurTypeOfOcenka.ToString() + "<br/>";
                    }
                    
                    
                }
            }
            catch
            {
                ResMessage = "Операция была завершена с ошибкой. Обратитесь к разработчикам";
            }

            if (ResMessage == "") ResMessage = "Ничего не сохранялось";

            return Json(ResMessage, JsonRequestBehavior.AllowGet);

        }

	    private class ItemHelper
	    {
            public string VedomCode {get; set; }
            public string StudentCode {get; set; }
            public string OcenkaCode {get; set; }
            public string OcenkaTypeCode {get; set; }
            public string Uchet { get; set; }
            public string StudentFIO { get; set; }  
        }

        [HttpPost]
	    public JsonResult CreateNewBegunok(int pVedCode, int pStudentId, DateTime pDateCreate, DateTime pDateGive, DateTime pDateExpired, int pOcenkaCode)
	    {
	        int LocalRes = -1;
	        string MessageRes = "";
            string LocDescr = "";
            string LocUser = "";

            if (Session["UserName"] != null)
            {
                // Что-нибудь делаем для авторизации пользователя
                LocUser = Session["UserName"].ToString();
            }
            else
            {
                LocUser = "Undefined, host: " + Request.UserHostName + "address: " + Request.UserHostAddress;
            }

            // Получаем список допустимых оценок для ведомости
            var TypesOfOcenki = context.GetOcenkiForVedom(pVedCode).ToArray();

            // Спрашиваем, попала ли оценка в требуемый диапазон
	        if (TypesOfOcenki.Any(item => item.КодИАСУ == pOcenkaCode))
	        {
                LocalRes = context.CreateNewBegunok(pVedCode, pStudentId, pDateCreate, pDateGive, pDateExpired, DateTime.Now,
                    pOcenkaCode, LocDescr, LocUser);
	        }
	        else
	        {
	            MessageRes = "Некорректный тип оценки. Бегунок не был создан";
	        }

	        if (LocalRes > 0)
	            MessageRes = "Создан бегунок";
	        else
	        {
	            MessageRes = "Бегунок не был создан";
	        }

	        return Json(MessageRes, JsonRequestBehavior.DenyGet);
	    }

        [HttpPost]
        public JsonResult EditBegunok(int pVedCode, int pBegunCode, int pStudentId, DateTime pDateCreate, DateTime pDateGive, DateTime pDateExpired, int pOcenkaCode)
        {
            //string str = Request.Url.AbsoluteUri;

            int LocalRes = -1;
            string MessageRes = "";
            string LocDescr = "";
            string LocUser = "";

            if (Session["UserName"] != null)
            {
                // Что-нибудь делаем для авторизации пользователя
                LocUser = Session["UserName"].ToString();  
            }
            else
            {
                LocUser = "Undefined, host: " + Request.UserHostName + "address: " + Request.UserHostAddress;
            }

            // Получаем список допустимых оценок для ведомости
            var TypesOfOcenki = context.GetOcenkiForVedom(pVedCode).ToArray();

            // Спрашиваем, попала ли оценка в требуемый диапазон
            if (TypesOfOcenki.Any(item => item.КодИАСУ == pOcenkaCode))
            {
                LocalRes = context.UpdateBegunok(pBegunCode, pStudentId, pVedCode, pDateCreate, pDateGive, pDateExpired, pOcenkaCode, LocUser);
            }
            else
            {
                MessageRes = "Некорректный тип оценки. Бегунок не был обновлен";
            }

            if (LocalRes > 1)
                MessageRes = "Бегунок обновлен";
            else
            {
                MessageRes = "Бегунок не был обновлен";
            }

            return Json(MessageRes, JsonRequestBehavior.DenyGet);
        }

        [HttpGet]
        public ActionResult PrintBegunok(int pBegunCode, int pVedCode)
        {
            // Получаем информацию по ведомости
            VedomDocument VedDescr = new VedomDocument(pVedCode, true);

            var dbResult = context.GetBegunDocument(pBegunCode);

            BegunDocumentItem begunok = (from item in dbResult
                                         where item.id == pBegunCode
                                         select item).First();
            String FIO = begunok.Фамилия + " " + begunok.Имя + " " + begunok.Отчество;
            DateTime DateGive = begunok.date_give;
            DateTime DateExpired = begunok.date_expired;
            int BegunNo = begunok.id;

            ViewBag.FIO = FIO;
            ViewBag.DateGive = DateGive.ToShortDateString();
            ViewBag.DateExpired = DateExpired.ToShortDateString();
            ViewBag.BegunNo = BegunNo;

            return View(VedDescr);
        }

        [HttpPost]
        public JsonResult ImportIASU(int pVedCode, int pVedTypeCode, int pKurs, int pSem, string pSource)
        {
            string ResForAction = "";

            ImportOcenki import = new ImportOcenki(pVedCode, pVedTypeCode, pKurs, pSem, pSource);

            ResForAction = import.ResMessage;

            return Json(ResForAction, JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public JsonResult ImportPreview(int pVedCode)
        {
            var dbResult = context.ImportPreView(pVedCode);

            var items = (from item in dbResult
                           select new
                           {
                               item.ved_type_code,
                               item.ved_type_name,
                               item.god,
                               item.kurs,
                               item.sem,
                               item.prep,
                               item.cont_count,
                               item.src
                           });

            return Json(items, JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public JsonResult CloseVedom(int pVedCode)
        {
            var dbRes = context.VedomCloseOpen(pVedCode, true);
            
            return Json(dbRes, JsonRequestBehavior.DenyGet);
        }
	}
}
