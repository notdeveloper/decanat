﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Decanat.Filters;

namespace Decanat.Controllers
{
    
    public class DefaultController : Controller
    {
        //
        // GET: /Default1/

        public ActionResult Index()
        {
            #region Опрос Cookies
            //var cookies = Request.Cookies["NCGTI_USER_NAME"];
            //string TargetRole = "Деканат";
            //string AuthAddress = "http://portal.ncgti.ru";
            //string ip = Request.UserHostAddress;

            //if ((ip.Contains("192.168")) || (ip.Contains("::"))) // ::1
            //    AuthAddress = "http://portal.ncgti.lan";

            //if (cookies != null)
            //{
            //    string UserName = cookies.Value.ToString();
            //    var context = new JournalEntities();
            //    var Roles = context.GetUserRoles(UserName).ToArray();
            //    if (Roles.Any(item => item.Role == TargetRole))
            //        ViewBag.AuthMessage = UserName + " : " + TargetRole; 
            //    else
            //        Response.Redirect(AuthAddress);
            //}
            //else
            //{
            //    ViewBag.AuthMessage = "нет пользователя";
            //    Response.Redirect(AuthAddress);
            //}
            #endregion // Опрос Cookies

            return View();
        }

        [HttpPost]
        public JsonResult GetGroupCollection()
        {

            #region Получение данных

            var context = new JournalEntities();
            var dbResult = context.GetGroupCollection(null, String.Empty, null, null, null, null, String.Empty).ToList();

            var groups = (from gr in dbResult select new
            {
                gr.Код,
                gr.Код_Факультета,
                gr.ФакСокращение,
                gr.Курс,
                gr.Название,
                gr.Учебный_План,
                gr.ФормаОбученияСокр,
                gr.ОКСО
            });

            //int a = 1;

            #endregion // Получение данных

            return Json(groups, JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public JsonResult GetVedomCollection(int pGrCode, String pPlanName)
        {

            #region Получение данных

            var context = new JournalEntities();

            var dbResult = context.GetVedomCollection(pGrCode, pPlanName);

            var vedoms = (from veditem in dbResult
                          select new
                          {
                              veditem.Дисциплина,
                              veditem.Описание, // тип  ведомости
                              veditem.Курс,
                              veditem.Семестр,
                              veditem.вед_Преподаватель,
                              veditem.вед_Код, // скрытый
                              veditem.ИмяФайла, // скытый
                              veditem.ТипВедомости,  // скрытый
                              veditem.ВидСессии, // скрытый
                              veditem.СеместрВыч, // Скрытыйы
                              veditem.IsCurrentAtt,
                              veditem.IsNeedReiting,
                              veditem.Закрыта
                          });

            // int a = 1;

            #endregion // Получение данных

            return Json(vedoms, JsonRequestBehavior.DenyGet);
        }


        public JsonResult AddNewVedom(int pGroupCode, string pPlanName, string pDiscipl, int pKurs, int pSem, int pSess, int pVedType)
        {
            var context = new JournalEntities();
            
            var message = "";

            try
            {
                var dbResult = context.AddNewVedom(pGroupCode, pPlanName, pDiscipl, pKurs, pSem, pSess, pVedType);
                message = "Success";
            }
            catch (Exception)
            {
                message = "Error";
            }

            return Json(message, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetStudCollection(int pGroupCode)
        {
            var context = new JournalEntities();

            var dbResult = context.GetStudentCollection(pGroupCode);

            var students = (from stud_item in dbResult select 
                                new {
                                    stud_item.Код,
                                    stud_item.ФИО,
                                    stud_item.Статус,
                                    stud_item.IsZadolzh
                                });

            int a = 0;

            return Json(students, JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetStudentZadolzh(int pStudentId)
        {
            var context = new JournalEntities();

            var dbResult = context.GetStudentZadolzh(pStudentId);

            var zadolzh = (from item in dbResult
                            select
                                new
                                {
                                    item.Дисциплина,
                                    item.Курс,
                                    item.Семестр,
                                    item.СеместрВыч,
                                    item.Сессия,
                                    item.вед_Преподаватель,
                                    item.вед_Код,
                                    item.ТипВедомости,
                                    item.Описание,
                                    item.Итог,
                                    item.Итоговая_Оценка,
                                    item.id,
                                    item.ocenka,
                                    item.isclosed,
                                    item.Примечание
                                });

            int a = 0;

            return Json(zadolzh, JsonRequestBehavior.DenyGet);
        }

        public JsonResult SaveBegunOcenka(int pBegunId, int pOcenkaCode)
        {
            string res = "ok";

            var context = new JournalEntities();

            var dbres = context.UpdateBegunokById(pBegunId, pOcenkaCode);

            return Json(res, JsonRequestBehavior.DenyGet);
        }

        public JsonResult CreateBegunForGroup(int pGroupCode)
        {
            string res = "ok";

            var context = new JournalEntities();

            var dbres = context.CreateNewBegunForGroup(pGroupCode);

            return Json(res, JsonRequestBehavior.DenyGet);
        }

    }
}
