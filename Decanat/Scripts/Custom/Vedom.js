﻿// Источники конроллеров
var urlOcenkiDataSource;
var urlBegunkiDataSourceRart1;
var urlBegunkiDataSourcePart2;
var urlSaveOcenki;
var urlCreateBegunok;
var urlEditBegunok;
var urlPrintBegunok;
// Адрес метода печати заполненной ведомости ведомости
var sourceUrlVedomPrint;

// Адрес метода для предварительного просмотра импорта
var sourceUrlImportPreview;

// Адрес метода импорта из ИАСУ
var sourceUrlVedomImport;

// Адрес метода закрытия ведомости
var sourceUrlVedomClose;

// переменные для импорта
var importVedTypeCode;
var importVedTypeName;
var importKurs;
var importSem;
var importSrc;
var importPrep;


// Диалог импорта
var importDialog;

/* Параметры текущей ведомости */
var curVedomCode;
var curStudentCode;
var curOcenkaCode;
var curOcenkaTypeCode;
var curUchet;
var curVedType;
var curStudentFio;
var isClosed, isCurrentAtt, isNeedReiting;

/* Текущий выбранный бегунок */
var curOperationType; // 0 - создание бегунка, 1 - редактирование бегунка
// Интерфейс
var curBegunRowIndex;
var curBegunNo;
// Динамические поля диалога
var itemBegunNo;
var itemBegunOcenka;
var itemBegunDateCreate;
var itemBegunDateGive;
var itemBegunDateExpired;

// Элементы управления ведомости
var begunGrid;
var ocenkiGrid;
var labelFio;
var btnSaveOcenki;
var btnCheckAll;
var messageDlg;
var waitWindow;
// Элементы управления бегунки
var btnNewBegun;
var btnEditBegun;
var btnPrintBegun;
var dlgBegunCaption;
var dlgBegun;

function InitSplitter(splitter) {
    splitter.jqxSplitter({
        width: '100%',
        height: '100%',
        panels: [{ size: '70%' }, { size: '30%' }]
    });
}

function InitVedomProperties() {
    var locIsClosed = $("#labelIsClosed");

    // отображаем закрыта / открыта
    if (isClosed == 0) {
        locIsClosed.html("Ведомость открыта");
        locIsClosed.attr('style', 'color:green;');
    }
    else {
        locIsClosed.html("Ведомость закыта");
        locIsClosed.attr('style', 'color:red;');
    }


    // Отображаем является ли аттестация текущей
    var locImageIsCurrentAtt = $("#imageIsCurrentAtt");

    if (isCurrentAtt == 1) { // текущая аттестация
        locImageIsCurrentAtt.attr('src', '/Content/Images/flag.png');
        locImageIsCurrentAtt.attr('title', 'Текушая аттестация');
    }
    else { // НЕ текущая аттестация
        locImageIsCurrentAtt.attr('src', '/Content/Images/flag_no.png');
        locImageIsCurrentAtt.attr('title', 'Не является текущей');
    }

    // Отображаем используется ли рейтинг
    var locImageIsNeedReiting = $("#imageIsNeedReiting");

    if ((isCurrentAtt == 1) && (isNeedReiting == 1)) { // Рейтинг учитывается
        locImageIsNeedReiting.attr('src', '/Content/Images/reiting.png');
        locImageIsNeedReiting.attr('title', 'Учитывается рейтинг');
    }
    else { // НЕ используется рейтинг
        locImageIsNeedReiting.attr('src', '/Content/Images/reiting_no.png');
        locImageIsNeedReiting.attr('title', 'Без учета рейтинга');

    }
}

function InitVedomGrid(ocenkiGrid) {
    // Построение интерфейса
    InitVedomGridInterface(ocenkiGrid);
    
    // Подключение данных
    InitVedomGridDataBind(ocenkiGrid);
}

function InitVedomGridInterface(ocenkiGrid) {

    ocenkiGrid.jqxGrid({
        columnsresize: true,
        selectionmode: 'singlerow',
        sortable: true,
        editable: true,
        columns: [
            { text: 'Учет', datafield: 'УчетВВед', columntype: 'checkbox', width: 40 },
            { text: 'ФИО', datafield: 'ФИО', editable: false, width: 250 },
            { text: '№ зачетки', datafield: 'Зачетка', editable: false, width: 80 },
            { text: 'Доступ', datafield: 'МожетПравить', editable: false, width: 40, cellsrenderer: renderDostup },
            {
                text: 'Код', datafield: 'ИнтИОц', width: 60, columntype: 'numberinput', cellsalign: 'center',
                cellbeginedit: beginEditOcenka,
                initeditor: function (row, cellvalue, editor) {
                    editor.jqxNumberInput({
                        decimalDigits: 0, spinButtons: false, min: 0, max: 9, readOnly: false
                    });

                    //editor.on('valuechanged', function (event) {
                    //    //alert('');
                    //    var value = event.args.value;
                    //});
                }
            },
            { text: 'Оценка', datafield: 'ИнтИОцО', editable: false, width: 180 },
            { text: 'Рейтинг', datafield: 'РейтинговыйБалл', editable: false, width: 80, cellsalign: 'center' },
            { text: 'Доп (оплата)', datafield: 'ДопускПоОплате', columntype: 'checkbox', editable: false, width: 100 },
            { text: 'Итоговая_Оценка', datafield: 'Итоговая_Оценка', editable: false, hidden: true }, // hidden
            { text: 'вс_Код', datafield: 'вс_Код', editable: false, hidden: true }, // hidden
            { text: 'оц_Код', datafield: 'оц_Код', editable: false, hidden: true } // hidden

        ],
        width: '100%',
        height: '95%'
    });
    
    $(ocenkiGrid).bind('rowselect', function (event) {
        studentSelected(event, ocenkiGrid);
    });
}

// beginEditOcenka
function beginEditOcenka(row, datafield, columntype, value) {

    var res = true;

    if (isClosed == 1) {
        return false;
    }

    var rdata = ocenkiGrid.jqxGrid('getrowdata', row);
    var IsEditable = rdata.МожетПравить;
    //alert(IsEditable);
    return IsEditable;
}

function renderDostup(row, column, value, defaultHtml, columnSettings, rowData) {
    if (value == true)
        return '<img src="/Content/Images/active_unlock.png" width="30px">';
    else return '<img src="/Content/Images/active_lock.png" width="30px">';
}
  
function studentSelected(event, ocenkiGrid) {
    
    var selectedIndex = event.args.rowindex;
    ocenkiGrid.jqxGrid('begincelledit', selectedIndex, 'ИнтИОц');
    
    curStudentCode = $(ocenkiGrid).jqxGrid('getcell', selectedIndex, 'вс_Код').value;
    curOcenkaCode = $(ocenkiGrid).jqxGrid('getcell', selectedIndex, 'оц_Код').value;
    curOcenkaTypeCode = $(ocenkiGrid).jqxGrid('getcell', selectedIndex, 'ИнтИОц').value;
    curUchet = $(ocenkiGrid).jqxGrid('getcell', selectedIndex, 'УчетВВед').value;
    
    curStudentFio = $(ocenkiGrid).jqxGrid('getcell', selectedIndex, 'ФИО').value;

    labelFio.text(curStudentFio);
    $("#itemBegunFIO").text(curStudentFio);

    // перегружаем таблицу с бегункам
    InitBegunGridDataBind(curStudentCode, curVedomCode);
}

function InitVedomGridDataBind(ocenkiGrid) {

    //alert(urlOcenkiDataSource);

    /*
    
    oc.оц_Код,
	oc.вс_Код,
	УчетВВед=oc.УчетВВед,
	ФИО=oc.Фамилия + " " + oc.Имя + " " + oc.Отчество,
	Зачетка=oc.Зачетка,
	oc.РейтинговыйБалл,
	oc.ДопускПоОплате,
    oc.Итоговая_Оценка,
    oc.Итог,
    oc.Рейтинг_Оценка,
    oc.ИнтИОц,
    oc.ИнтИОцО,
    oc.ИнтИтогО,
    oc.МожетПравить    

    */

    var source =
    {
        datatype: 'json',
        type: 'GET',
        datafields:
        [
            { name: 'оц_Код', type: 'int' },
            { name: 'вс_Код', type: 'int' },
            { name: 'УчетВВед', type: 'bool' },
            { name: 'ФИО', type: 'string' },
            { name: 'Зачетка', type: 'string' },
            { name: 'РейтинговыйБалл', type: 'int' },
            { name: 'ДопускПоОплате', type: 'bool' },
            { name: 'Итоговая_Оценка', type: 'int' },
            { name: 'Итог', type: 'int' },
            { name: 'Рейтинг_Оценка', type: 'int' },
            { name: 'ИнтИОц', type: 'int' },
            { name: 'ИнтИОцО', type: 'string' },
            { name: 'ИнтИтогО', type: 'string' },
            { name: 'МожетПравить', type: 'bool' }
        
        ],
        url: urlOcenkiDataSource
    };

    var dataAdapter = new $.jqx.dataAdapter(source,
    {
        loadComplete: function () {
            loadCompleteOcenki(ocenkiGrid, dataAdapter);
        }
    });
    
    // Присоединяем данные к таблице
    $(ocenkiGrid).jqxGrid({ source: dataAdapter });
}

function loadCompleteOcenki(ocenkiGrid, dataAdapter) {
    //alert('11');
    
    // Пытаемся выделить первую строку
    var records = dataAdapter.records;
    var rowcount = records.length;
    if (rowcount > 0)
        $(ocenkiGrid).jqxGrid('selectrow', 0);
}

/* Создание таблицы бегунков */
function InitBegunGrid(begunGrid) {
    InitBegunGridInterface(begunGrid);
}

/* Инициализация внешнего вида таблицы бегунков */
function InitBegunGridInterface(begunGrid) {
    begunGrid.jqxGrid({
        columnsresize: true,
        selectionmode: 'singlerow',
        sortable: true,
        columns: [
            { text: 'Оценка', datafield: 'ocenka', width: 80 },
            { text: '№', datafield: 'id', width: 80 },
            { text: 'Закрыт', datafield: 'isclosed', width: 40, cellsrenderer: renderDostupBegun },
            { text: 'Выдан', datafield: 'date_give', width: 100, cellsformat: 'dd/MM/yyyy' },
            { text: 'Срок до', datafield: 'date_expired', width: 100, cellsformat: 'dd/MM/yyyy' },
            { text: 'id_stud', datafield: 'id_stud', hidden: true }, // hidden
            { text: 'date_create', datafield: 'date_create', hidden: true }, // hidden
            { text: 'id_ved', datafield: 'id_ved', hidden: true }, // hidden
            { text: 'date_lastupdate', datafield: 'date_lastupdate', hidden: true }, // hidden
            { text: 'description', datafield: 'description', hidden: true }, // hidden
            { text: 'user_last', datafield: 'user_last', hidden: true } // hidden
        ],
        width: '100%',
        height: '80%'
    });
    
    $(begunGrid).bind('rowselect', function (event) {
        begunSelected(event, begunGrid);
        //curBegunIndex = event.args.rowindex;
    });
}


function renderDostupBegun(row, column, value, defaultHtml, columnSettings, rowData) {
    if (value == false)
        return '<img src="/Content/Images/active_unlock.png" width="30px">';
    else return '<img src="/Content/Images/active_lock.png" width="30px">';
}

function begunSelected(event, begunGrid) {
    // Устанавливаем поля интерфейса
    curBegunRowIndex = event.args.rowindex;
    
    // Устанавливаем текущий номер бегунка 
    curBegunNo = $(begunGrid).jqxGrid('getcell', curBegunRowIndex, 'id').value;
    $("#labelBegunNo").text(curBegunNo);
    
    // Устанавливаем текущую ссылку бегунка
    itemBegunNo = curBegunNo;
    itemBegunOcenka = $(begunGrid).jqxGrid('getcell', curBegunRowIndex, 'ocenka').value;
    
    itemBegunDateGive = $(begunGrid).jqxGrid('getcell', curBegunRowIndex, 'date_give').value;
    //alert(itemBegunDateGive);
    itemBegunDateExpired = $(begunGrid).jqxGrid('getcell', curBegunRowIndex, 'date_expired').value;

    // Открытие кнопок работы с бегунками
    $("#btnEditBegun").jqxButton({ disabled: false });
    $("#btnPrintBegun").jqxButton({ disabled: false });
}

/* Загрузка данных таблицы бегунков */
function InitBegunGridDataBind(studentId, vedomCode) {
    // Задаем источник
    var curUrl = urlBegunkiDataSourceRart1.replace("param1", studentId);
    curUrl = curUrl.replace("param2", vedomCode);
    curUrl = curUrl.replace("amp;", "");
    //alert(curUrl);
    var source =
    {
        datatype: 'json',
        type: 'GET',
        datafields:
        [
            { name: 'id', type: 'int' },
            { name: 'id_ved', type: 'int' },
            { name: 'id_stud', type: 'int' },
            { name: 'date_create', type: 'date' },
            { name: 'date_give', type: 'date' },
            { name: 'date_expired', type: 'date' },
            { name: 'date_lastupdate', type: 'date' },
            { name: 'ocenka', type: 'int' },
            { name: 'description', type: 'string' },
            { name: 'user_last', type: 'string' },
            { name: 'isclosed', type: 'bool' }
        ],
        url: curUrl
    };
    
    var dataAdapter = new $.jqx.dataAdapter(source,
    {
        loadComplete: function () {
            loadCompleteBegunki(begunGrid, dataAdapter);
        }
    });

    // Присоединяем источник к таблице
    begunGrid.jqxGrid({ source: dataAdapter });
}

function loadCompleteBegunki(begunGrid, dataAdapter) {

    // Закрытие кнопок работы с бегунками
    $("#btnEditBegun").jqxButton({ disabled: true });
    $("#btnPrintBegun").jqxButton({ disabled: true });

    // Пытаемся выделить первую строку
    var records = dataAdapter.records;
    var rowcount = records.length;
    if (rowcount > 0) {
        $(begunGrid).jqxGrid('selectrow', 0);
    }
}

function initImportDialog() {
    // Создаем окно импорта
    importDialog = $("#importDialog");
    importDialog.jqxWindow({
        height: 280,
        width: 600,
        resizable: false,
        isModal: true,
        autoOpen: false,
        initContent: function () {
            $('#importOk').jqxButton({ width: '65px' });
            $('#importCancel').jqxButton({ width: '65px' });
            $('#importOk').focus();
        }
    });
    
    // Подключаем обработчики
    $('#importOk').on('click', function () {
        importDialog.jqxWindow('close');
        onImportIASU();
    });

    $('#importCancel').on('click', function () {
        importDialog.jqxWindow('close');
    });
    
    // Создаем интерфей таблицы
    var locGrid = $("#importGrid").jqxGrid({
        columnsresize: true,
        selectionmode: 'singlerow',
        sortable: false,
        editable: false,
        columns: [
            { text: 'Тип', datafield: 'ved_type_name', width: 80 },
            { text: 'Год', datafield: 'god', width: 80 },
            { text: 'Курс', datafield: 'kurs', editable: false, width: 60 },
            { text: 'Семестр', datafield: 'sem', editable: false, width: 60 },
            { text: 'Преподаватель', datafield: 'prep', width: 200 },
            { text: 'Контингент', datafield: 'cont_count', width: 60, cellsalign: 'center' },
            { text: 'Источник', datafield: 'src', width: 40 },
            { text: 'ved_type_code', datafield: 'ved_type_code', hidden:true } // hidden
        ],
        width: '100%',
        height: '150'
    });
    
    locGrid.bind('rowselect', function (event) {
        importItemSelected(event, locGrid);
        //curBegunIndex = event.args.rowindex;
    });

}

function importItemSelected(event, grid) {
    var curImportIndex = event.args.rowindex;
    
    importVedTypeCode = grid.jqxGrid('getcell', curImportIndex, 'ved_type_code').value;
    importKurs = grid.jqxGrid('getcell', curImportIndex, 'kurs').value;
    importSem = grid.jqxGrid('getcell', curImportIndex, 'sem').value;
    importSrc = grid.jqxGrid('getcell', curImportIndex, 'src').value;
    importPrep = grid.jqxGrid('getcell', curImportIndex, 'prep').value;
    importVedTypeName = grid.jqxGrid('getcell', curImportIndex, 'ved_type_name').value;
    
    $("#importCaption").html('Импорт: ' + importKurs + ' / ' + importSem + ' / ' + importVedTypeName);
    //alert(curVedomTypeCode);
}

function initButtons() {

    btnSaveOcenki.jqxButton({ width: 100, height: 25 });
    $(btnSaveOcenki).on('click',
        function () {
            onSaveAllVedom();
        });

    //btnCheckAll
    // Выбираем все флажки
    btnCheckAll.jqxButton({ width: 80, height: 25 });
    $(btnCheckAll).on('click',
    function () {
        //alert('Check all');
        var rowscount = $(ocenkiGrid).jqxGrid('getdatainformation').rowscount;
        $(ocenkiGrid).jqxGrid('beginupdate');
        for (var i = 0; i < rowscount; i++) {
            $(ocenkiGrid).jqxGrid('setcellvalue', i, 'УчетВВед', true, false);
        }
        $(ocenkiGrid).jqxGrid('endupdate');
    });
    
    var lnkPrintBlank = $("a#lnkPrintBlank");
    $(lnkPrintBlank).jqxLinkButton({ width: '80', height: '25' });
    var curRef = sourceUrlVedomPrint.replace("amp;", "");
    curRef = curRef.replace("param1", curVedomCode);
    curRef = curRef.replace("param2", 0);
    lnkPrintBlank.jqxLinkButton("href", curRef);

    // Новый бегунок
    btnNewBegun = $("#btnNewBegun").jqxButton({ width: 80, height: 25 });
    $(btnNewBegun).on('click',
    function () {
        onNewBegun();
    });
    
    // Редактирование бегунка
    btnEditBegun = $("#btnEditBegun").jqxButton({ width: 80, height: 25 });
    $(btnEditBegun).on('click',
    function () {
        onEditBegun();
    });
    
    // Печать бегунка
    btnPrintBegun = $("#btnPrintBegun").jqxButton({ width: 80, height: 25 });
    $(btnPrintBegun).on('click',
    function () {
        onPrintBegun();
    });

    // Перенос оценок из ИАСУ btnImportIASU
    var locImportIASU = $("#btnImportIASU").jqxButton({ width: 80, height: 25 });
    $(locImportIASU).on('click',
        function () {
            // onImportIASU();
            onImportPreview();
        });

    // Закрыть ведомость btnCloseVedom
    btnCloseVedom = $("#btnCloseVedom").jqxButton({ width: 70, height: 25 });
    $(btnCloseVedom).on('click',
    function () {
        onCloseVedom();
    });
}

function onCloseVedom() {
    var locUrl = sourceUrlVedomClose;
    // вызываем метод сервера
    $.ajax({
        type: "POST",
        url: locUrl,
        //contentType: "application/json; charset=utf-8",
        data: { 'pVedCode': curVedomCode },
        dataType: "json",
        success: function (res) {
            // если успешно обновляем страницу
            location.reload();
        },
        error: function (x, e) {
            // заполняем диалоговое окно 
            $(messageDlgContent).html(x.status + ' - ' + e);
            // Открываем окно сообщений
            $(messageDlg).jqxWindow('open');
        }
    });


}

function onImportPreview() {
    // Задаем источник
    var curUrl = sourceUrlImportPreview.replace("param1", curVedomCode);
    //alert(curUrl);
    var source =
    {
        datatype: 'json',
        type: 'POST',
        datafields:
        [
            { name: 'ved_type_code', type: 'int' },
            { name: 'ved_type_name', type: 'string' },
            { name: 'god', type: 'string' },
            { name: 'kurs', type: 'int' },
            { name: 'sem', type: 'int' },
            { name: 'prep', type: 'string' },
            { name: 'cont_count', type: 'int' },
            { name: 'src', type: 'string' }
        ],
        url: curUrl
    };

    var importDA = new $.jqx.dataAdapter(source,
    {
        loadComplete: function () {
            loadCompleteImportPreview(importDA);
        }
    });

    // Присоединяем источник к таблице
    $("#importGrid").jqxGrid({ source: importDA });

    /* Показываем диалоговое окно */
    importDialog.jqxWindow('open');
    

}

function loadCompleteImportPreview(da) {
    var records = da.records;
    var rowcount = records.length;
    if (rowcount > 0) {
        $("#importGrid").jqxGrid('selectrow', 0);
    }
}

function onImportIASU() {

    // Создание спец. объекта
    var pImportParams = {
        pVedCode: curVedomCode,
        pVedTypeCode: importVedTypeCode,
        pKurs: importKurs,
        pSem: importSem,
        pSource: importSrc
    };

    var locUrl = sourceUrlVedomImport;
    /*int pVedCode, int pVedTypeCode, int pKurs, int pSem, string pSource*/
    // alert(locUrl + '-' + JSON.stringify(pImportParams));

    // блокируем интерфейс
    waitWindow.jqxWindow('open');
    $.ajax({
        type: "POST",
        url: locUrl,
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(pImportParams),
        dataType: "json",
        success: function (res) {
            $(ocenkiGrid).jqxGrid('updatebounddata');
            // заполняем диалоговое окно 
            $(messageDlgContent).html(res);
            // Закрываем окно одидания
            waitWindow.jqxWindow('close');
            // Открываем окно сообщений
            $(messageDlg).jqxWindow('open');
        },
        error: function (x, e) {
            //$(ocenkiGrid).jqxGrid('updatebounddata');
            // заполняем диалоговое окно 
            $(messageDlgContent).html(x.status + ' - ' + e);
            // Закрываем окно одидания
            waitWindow.jqxWindow('close');
            // Открываем окно сообщений
            $(messageDlg).jqxWindow('open');
        }
    });

}

function onPrintBegun() {

    var locUrl = urlPrintBegunok.replace("amp;", "");
    locUrl = locUrl.replace("param1", curBegunNo);
    locUrl = locUrl.replace("param2", curVedomCode);

    window.open(locUrl, "_blank");
    
}

function onNewBegun() {
    
    // Устанавливаем тип команды
    curOperationType = 0;

    dlgBegunCaption.html('Создание экзаменационного листа');
    // Инициализируем контролы перед показом
    itemBegunNo = '--';
    itemBegunDateCreate = new Date();
    itemBegunDateGive = itemBegunDateCreate;
    itemBegunDateExpired = new Date();
    itemBegunDateExpired.setDate(itemBegunDateGive.getDate() + 3);
    itemBegunOcenka = 0;
    // Показываем диалог
    preInitDialogBegun();
    dlgBegun.jqxWindow('open');
}

function onEditBegun() {
    
    // Устанавливаем тип команды
    curOperationType = 1;

    dlgBegunCaption.html('Редактирование (ввод оценки)');

    // Инициализируем контролы перед показом
    itemBegunNo = $(begunGrid).jqxGrid('getcell', curBegunRowIndex, 'id').value;
    itemBegunOcenka = $(begunGrid).jqxGrid('getcell', curBegunRowIndex, 'ocenka').value;
    itemBegunDateCreate = $(begunGrid).jqxGrid('getcell', curBegunRowIndex, 'date_create').value;
    itemBegunDateGive = $(begunGrid).jqxGrid('getcell', curBegunRowIndex, 'date_give').value;   
    itemBegunDateExpired = $(begunGrid).jqxGrid('getcell', curBegunRowIndex, 'date_expired').value;

    //alert('preinit');
    preInitDialogBegun();
    dlgBegun.jqxWindow('open');
}

function InitDialogBegun() {

    dlgBegun.jqxWindow({
        height: 430,
        width: 600,
        resizable: false,
        isModal: true,
        autoOpen: false,
        initContent: function () {
            // Кнопочки
            $('#okBegun').jqxButton({ width: '150px' });
            $('#cancelBegun').jqxButton({ width: '150px' });            
            //Устанавливаем поле для оценки itemBegunOcenka
            $("#itemBegunOcenka").jqxNumberInput({ min: 0, max: 9, decimalDigits: 0, width: '100px', height: '25px', promptChar: '_', digits:1 });
        }
    });
    
    $('#okBegun').on('click', function () {
        onSaveBegun();
        dlgBegun.jqxWindow('close');
    });
    $('#cancelBegun').on('click', function () { dlgBegun.jqxWindow('close'); });
}

function preInitDialogBegun() {

    // Установим номер бегунка
    $('#itemBegunNo').text(itemBegunNo);

    // Установим оценку
    $("#itemBegunOcenka").val(itemBegunOcenka);

    // Создаем и устанавливаем даты
    $('#itemBegunDateCreate').jqxDateTimeInput({ width: '150px', height: '25px', formatString: 'dd/MM/yyyy' });
    $('#itemBegunDateCreate').jqxDateTimeInput('setDate', itemBegunDateCreate);

    $('#itemBegunDateGive').jqxDateTimeInput({ width: '150px', height: '25px', formatString: 'dd/MM/yyyy' });
    $('#itemBegunDateGive').jqxDateTimeInput('setDate', itemBegunDateGive);

    $('#itemBegunDateExpired').jqxDateTimeInput({ width: '150px', height: '25px', formatString: 'dd/MM/yyyy' });
    $('#itemBegunDateExpired').jqxDateTimeInput('setDate', itemBegunDateExpired);

    if (curOperationType == 1) {
        //alert('in');
        $('#itemBegunDateCreate').jqxDateTimeInput({ disabled: true });
        $('#itemBegunDateGive').jqxDateTimeInput({ disabled: true });
        $('#itemBegunDateExpired').jqxDateTimeInput({ disabled: true });
    }
    else {
        $('#itemBegunDateCreate').jqxDateTimeInput({ disabled: false });
        $('#itemBegunDateGive').jqxDateTimeInput({ disabled: false });
        $('#itemBegunDateExpired').jqxDateTimeInput({ disabled: false });
    }
    
    //$('#itemBegunOcenka').jqxNumberInput(itemBegunOcenka);
}

function onSaveBegun() {
    
    // Получаем все необходимые переменные
    var locUrl = urlCreateBegunok;
    
    var locVedCode = curVedomCode;
    var locStudentCode = curStudentCode;
    var locDateCreate = $('#itemBegunDateCreate').jqxDateTimeInput('getDate');
    var locDateGive = $('#itemBegunDateGive').jqxDateTimeInput('getDate');
    var locDateExpired = $('#itemBegunDateExpired').jqxDateTimeInput('getDate');
    var locOcenkaCode = $("#itemBegunOcenka").val();

    if (curOperationType == 0) {
        
        // Создание спец. объекта
        var pObCreate = {
            pVedCode: locVedCode,
            pStudentId: locStudentCode,
            pDateCreate: locDateCreate,
            pDateGive: locDateGive,
            pDateExpired: locDateExpired,
            pOcenkaCode: locOcenkaCode
        };

        //alert(locUrl + '-' + JSON.stringify(pObCreate));

        // Сохранение
        $.ajax({
            type: "POST",
            url: locUrl,
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(pObCreate),
            dataType: "json",
            success: function (res) {
                $(begunGrid).jqxGrid('updatebounddata');
            },
            error: function (x, e) {
                alert('error');
            }
        });
    }

    if (curOperationType == 1) {
        // Устанавливаем другой тип источника
        locUrl = urlEditBegunok;
        
        
        // Создание спец. объекта для редактирования
        var pObEdit = {
            pVedCode: locVedCode,
            pBegunCode: curBegunNo,
            pStudentId: curStudentCode,
            pDateCreate: locDateCreate,
            pDateGive: locDateGive,
            pDateExpired: locDateExpired,
            pOcenkaCode: locOcenkaCode
        };

        //alert(locUrl + '-' + JSON.stringify(pObEdit));
        

        // Сохранение
        $.ajax({
            type: "POST",
            url: locUrl,
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(pObEdit),
            dataType: "json",
            success: function () {
                $(begunGrid).jqxGrid('updatebounddata');
            },
            error: function () {
                alert('error');
            }
        });
    }

    
}

function onSaveAllVedom() {
    /* Сохраняем все оценки для данной ведомости */

    var recCount = $(ocenkiGrid).jqxGrid('getboundrows').length;
    //alert(recCount);
    // Устанавливаем параметры
    var locStudentCode = null;
    var locOcenkaCode = null;
    var locOcenkaTypeCode = null;
    var locUchet = null;
    var locUser = 'undef';

    var arOcenki = [];
    var item;

    //recCount = 6;
    // Формируем массив с оценками
    for (var i = 0; i < recCount; i++) {
        arOcenki.push({
            VedomCode: curVedomCode,
            StudentCode : $(ocenkiGrid).jqxGrid('getcell', i, 'вс_Код').value,
            OcenkaCode : $(ocenkiGrid).jqxGrid('getcell', i, 'оц_Код').value,
            OcenkaTypeCode: $(ocenkiGrid).jqxGrid('getcell', i, 'ИнтИОц').value,
            Uchet: $(ocenkiGrid).jqxGrid('getcell', i, 'УчетВВед').value, 
            StudentFIO: $(ocenkiGrid).jqxGrid('getcell', i, 'ФИО').value
        });
    }
    
    // Блокируем интерфейс пользователя
    waitWindow.jqxWindow('open');

    // Передаем массив с оценками в функцию отправки
    SaveAllOcenki(arOcenki);

    //$(ocenkiGrid).jqxGrid('updatebounddata'); 
}

function SaveAllOcenki(ocenkiArray) {
    //alert(curVedCode + ' // ' + locStudentCode + ' // ' + locOcenkaCode + ' // ' + locOcenkaTypeCode + ' // ' + locUchet + ' // ' + locUser);
    //alert(urlSaveOcenki);
    //alert(JSON.stringify(ocenkiArray));

    $.ajax({
        type: "POST",
        url: urlSaveOcenki,
        //contentType: "application/json; charset=utf-8",
        data: { OcenkiItems: JSON.stringify(ocenkiArray) },
        //dataType: "json",
        success: function (res) {
            //alert(res);
            $(ocenkiGrid).jqxGrid('updatebounddata');
            $(messageDlgContent).html(res);
            waitWindow.jqxWindow('close');
            $(messageDlg).jqxWindow('open');
        },
        error: function (x, e) {
            $(ocenkiGrid).jqxGrid('updatebounddata');
            $(messageDlgContent).html(x.status + ' - ' + e);
            waitWindow.jqxWindow('close');
            $(messageDlg).jqxWindow('open');
        }
    });
}

