﻿// источники
var sourceUrlGroups;
var sourceUrlVedom;
var sourceUrlVedomAdd;
var sourceUrlVedomEdit;
var sourceUrlVedomPrint;
var sourceUrlVedomCollection;
var sourceUrlStudents;
var sourceUrlZadolzh;
var sourceUrlSaveBegun;
var sourceUrlPrintBegun;
var sourceUrlCreateBegunForGroup;

// Элементы управления
var vedomGrid;
var groupGrid;
var labelDepartment;
var labelGroupName;
var labelOKCO;
var labelFO;
var labelKurs;
var labelPlanName;
var mainTabs; // Вкладки для разделения бегунков/ ведомостей
var splitterContBegun; // Splitter для разделения списка студентов и бегунков
var studentGrid; // Grid для вывода студентов группы
var begunGrid; // Grid для вывода бегунков/ задолженностей студентов
///////////////////////
var labelDiscipl;
var labelKursVedom;
var labelSemVedom;
var labelTypeVedom;
var labelSess;
var labelSemCalc;
var labelVedomNo;
///////////////////////
var btnNewVedom;
var lnkEditVedom;
var lnkPrintBlank;
var lnkPrintEmpty;
var btnCreateBeguns;
var btnEditSelBegun;
var btnPrintSelBegun;
var dlgEditSelBegun;

/* Переменные для организации запроса на получение ведомостей */
var tmplVedomSourse_1 = "http://localhost:8599/Default/GetVedomCollection?pGrCode=";
var tmplVedomSourse_2 = "&pPlanName=";

/* Текущие переменные */
var curGroupCode;
var curGroupName;
var curPlanName;
var curKurs;
var curOKCO;
var curFO;
var curDepartmentName;

//////////////////////////////////
var curVedCode;
var curVedTypeCode;
//////////////////////////////////
var curStudId;
var curStudFIO;
var curStudStatus;
var curIsStudZadolzh;
var curBegunCode;
var curBegunVedCode;

function InitSplitter(splitter) {

    splitter.jqxSplitter({
        width: '100%',
        height: '100%',
        panels: [{ size: '30%', min: 250 }, { size: '70%' }]
    });
}

function InitGroupGrid(groupGrid, sourceUrl) {

    var rowcount = 0;

    var source =
        {
            datatype: "json",
            type: "POST",
            datafields: [
                { name: 'Код', type: 'int' },
                { name: 'Код_Факультета', type: 'int' },
                { name: 'ФакСокращение', type: 'string' },
                { name: 'Курс', type: 'int' },
                { name: 'Название', type: 'string' },
                { name: 'Учебный_План', type: 'string' },
                { name: 'ФормаОбученияСокр', type: 'string' },
                { name: 'ОКСО', type: 'string' }
            ],
            url: sourceUrl
        };
    
    var dataAdapter = new $.jqx.dataAdapter(source, {
        loadComplete: function() {
            loadCompleteGroups(groupGrid, dataAdapter);
        }
        
    });

    /* Создание таблицы с группами */
    groupGrid.jqxGrid({
        source: dataAdapter,
        sortable: true,
        columnsresize: true,
        filterable: true,
        showfilterrow: true,
        selectionmode: 'singlerow',
        columns: [
            { text: 'Группа', datafield: "Название", width: 200 },
            { text: 'Курс', datafield: "Курс", width: 60, filtertype: 'checkedlist' },
            { text: 'Факультет', datafield: "ФакСокращение", width: 100, filtertype: 'checkedlist' },
            { text: 'План', datafield: "Учебный_План", width: 50, hidden:true }, // hidden
            { text: 'Kod', datafield: "Код", hidden: true }, // скрытый
            { text: 'ОКСО', datafield: "ОКСО", hidden: true }, // скрытый
            { text: 'ФО', datafield: "ФормаОбученияСокр", width: 50, filtertype: 'checkedlist' } // скрытый
            
        ],
        width: '100%',
        height: '95%'
    });
    
    
    // Подключаем обраюотчик выделения строки
    $(groupGrid).bind('rowselect', function (event) {
        groupSelected(event);
    });

}

function loadCompleteGroups(groupGrid, dataAdapter) {

    // выделяем строку
    var records = dataAdapter.records;
    var rowcount = records.length;
    if(rowcount>0)
        $(groupGrid).jqxGrid('selectrow', 0);
}

function groupSelected(event) {
    var selectedRowIndex = event.args.rowindex;

    // Получем данные из текущей строки
    curPlanName = $(groupGrid).jqxGrid('getcell', selectedRowIndex, 'Учебный_План').value;
    curGroupCode = $(groupGrid).jqxGrid('getcell', selectedRowIndex, 'Код').value;
    
    labelPlanName.text(curPlanName);

    curGroupName = $(groupGrid).jqxGrid('getcell', selectedRowIndex, 'Название').value;
    labelGroupName.text(curGroupName);
    
    curOKCO = $(groupGrid).jqxGrid('getcell', selectedRowIndex, 'ОКСО').value;
    labelOKCO.text(curOKCO);
    
    curFO = $(groupGrid).jqxGrid('getcell', selectedRowIndex, 'ФормаОбученияСокр').value;
    labelFO.text(curFO);
    
    curKurs = $(groupGrid).jqxGrid('getcell', selectedRowIndex, 'Курс').value;
    labelKurs.text(curKurs);
    
    curDepartmentName = $(groupGrid).jqxGrid('getcell', selectedRowIndex, 'ФакСокращение').value;
    labelDepartment.text(curDepartmentName);

    // Узнаем номер текущей выбранной вкладки
    var selectedItem = $("#mainTabs").jqxTabs('val'); 

    //alert(selectedItem);

    // Ребиндим данные в теблице ведомостей
    if (selectedItem == 0){
        InitVedomGrid_DataBind(vedomGrid, curGroupCode, curPlanName);
    }

    // Ребиндим данные в таблице студентов
    if (selectedItem == 1) {
        InitStudentGrid_DataBind(curGroupCode);
    }
}

function InitVedomGrid(vedomGrid) {

    // Инициализация внешнего вида таблицы
    InitVedomGrid_Interface(vedomGrid);
}

function InitVedomGrid_Interface(vedomGrid) {
    
    vedomGrid.jqxGrid({
        columnsresize: true,
        filterable: true,
        showfilterrow: true,
        selectionmode: 'singlerow',
        sortable: true,
        columns: [
            //{ text: 'Ст', width: '20', cellsrenderer: vedomStatusCellRenderer, filterable: false },
            { text: 'Дисциплина', datafield: "Дисциплина", width: '380' },
            { text: 'А', datafield: "IsCurrentAtt", width: '25', filterable: false, cellsrenderer: vedomStatusCellRenderer, cellsalign: 'center' },
            { text: 'Д', datafield: "Закрыта", width: '25', filterable: false, cellsrenderer: vedomDostupCellRenderer, cellsalign: 'center' },
            { text: 'Р', datafield: "IsNeedReiting", width: '25', filterable: false, cellsrenderer: vedomReitingCellRenderer, cellsalign: 'center' },
            { text: 'Вид', datafield: "Описание", width: 70, filtertype: 'checkedlist' },
            { text: 'Курс', datafield: "Курс", width: 60, filtertype: 'checkedlist', cellsalign: 'center' },
            { text: 'Семестр', datafield: "СеместрВыч", width: 60, filtertype: 'checkedlist', cellsalign: 'center' },
            { text: 'Преподаватель', datafield: "вед_Преподаватель", width: 150 },
            { text: '№', datafield: "вед_Код", width: 80 },
            { text: 'Сессия', datafield: "ВидСессии", width: 50, hidden: true }, // hidden
            { text: 'Имя плана', datafield: "ИмяФайла", width: 50, hidden: true }, // скрытый
            { text: 'Код типа ведомости', datafield: "ТипВедомости", width: 50, hidden: true }, // скрытый
            { text: 'СеместрНорм', datafield: 'Семестр', hidden: true } // { name: 'СеместрВыч', type: 'int' } 

        ],
        width: '100%',
        height: '80%',
    });
    
    // Подключаем обраюотчик выделения строки
    $(vedomGrid).bind('rowselect', function (event) {
        vedomSelected(event);
    });   
}

function vedomDostupCellRenderer(row, column, value, defaultHtml, columnSettings, rowData) {

    if (!rowData.вед_Код) {
        // Если ведомость не создана
        return '<span style="color: #000000;">-</span>';
    }
    else {
        // Если ведомость создана
        if (rowData.IsCurrentAtt) {
            // Если текущая
            if (rowData.Закрыта)
                return '<img src="/Content/Images/active_lock.png" width="20px">';
            else
                return '<img src="/Content/Images/active_unlock.png" width="20px">';

        }
        else {
            // Если НЕ текущая
            if (rowData.Закрыта)
                return '<img src="/Content/Images/noactive_lock.png" width="20px">';
            else
                return '<img src="/Content/Images/noactive_unlock.png" width="20px">';
        }
    }
}

function vedomStatusCellRenderer(row, column, value, defaultHtml, columnSettings, rowData) {

    if (!rowData.вед_Код) {
        // Если ведомость не создана
        return '<span style="color: #000000;">-</span>';
    }
    else {
        // Если ведомость создана
        if (rowData.IsCurrentAtt) {
            // Если текущая
            return '<img src="/Content/Images/flag.png" width="20px">';
        }
        else {
            // Если НЕ текущая
            return '<span style="color: #0000ff;">-</span>';
        }
    }    
}

function vedomReitingCellRenderer(row, column, value, defaultHtml, columnSettings, rowData) {

    if (!rowData.вед_Код) {
        // Если ведомость не создана
        return '<span style="color: #000000;">-</span>';
    }
    else {
        // Если ведомость создана
        if (rowData.IsCurrentAtt) {
            // Если текущая аттестация
            if (rowData.IsNeedReiting)
                return '<img src="/Content/Images/reiting.png" width="20px">';
            else return '<span style="color: #0000ff;">-</span>';
        }
        else {
            // Если НЕ текущая аттестация
            return '<span style="color: #0000ff;">-</span>';
        }
    }
}

/* Инициализация элементов интерфейса: вкладки и splitter*/
function initInterfaceZadolzh() {
    
    // Делаем вкладки
    mainTabs.jqxTabs({ width: '100%', height: '500', position: 'top' });
    mainTabs.on('tabclick', onTabZadolzhClick);
    
    // делаем splitter
    splitterContBegun.jqxSplitter({
        width: '100%',
        height: '100%',
        panels: [{ size: '20%', min: 40 }, { size: '80%' }]
    });
    
    /* Создание кнопок для работы с бегунками */

    // Создание бегунков на группы
    btnCreateBeguns = $("#btnCreateBeguns").jqxButton({ width: 80, height: 25 });
    $(btnCreateBeguns).on('click',
    function () {
        onCreateBeguns();
    });

    // Редактирование бегунка
    btnEditSelBegun = $("#btnEditSelBegun").jqxButton({ width: 80, height: 25 });
    $(btnEditSelBegun).on('click',
    function () {
        onEditSelBegun();
    });

    // Печать бегунка
    btnPrintSelBegun = $("#btnPrintSelBegun").jqxButton({ width: 80, height: 25 });
    $(btnPrintSelBegun).on('click',
    function () {
        onPrintSelBegun();
    });
}

function onTabZadolzhClick(event) {
    var clickedItem = event.args.item;
    //alert(clickedItem);
    if (clickedItem == 0) {
        // обновляем ведомости
        InitVedomGrid_DataBind(vedomGrid, curGroupCode, curPlanName);
    } else {
        // обновляем бегунки
        InitStudentGrid_DataBind(curGroupCode);
    }
}

    function onCreateBeguns() {
    //alert('create beguns');
    var locUrl = sourceUrlCreateBegunForGroup;

    $("#dlgWait").jqxWindow('open');

    // Сохранение
    $.ajax({
        type: "POST",
        url: locUrl,
        //contentType: "application/json; charset=utf-8",
        data: { 'pGroupCode': curGroupCode },
        dataType: "json",
        success: function (res) {
            $(studentGrid).jqxGrid('updatebounddata');
            $("#dlgWait").jqxWindow('close');
        },
        error: function (x, e) {
            alert('error');
            $("#dlgWait").jqxWindow('close');
        }
    });
}

function onEditSelBegun() {
    $('#dlgEditSelBegunCaption').text('Редактирование бегунка №'+curBegunCode);
    dlgEditSelBegun.jqxWindow('open');
}

function onPrintSelBegun() {
    // Печать
    var locUrl = sourceUrlPrintBegun.replace("amp;", "");
    locUrl = locUrl.replace("param1", curBegunCode);
    locUrl = locUrl.replace("param2", curBegunVedCode);

    window.open(locUrl, "_blank");
}

/* Инициализация интерфейса Grid со студентами */
function studentGridInit_Interface() {
    studentGrid.jqxGrid({
        //source: dataAdapter,
        sortable: true,
        columnsresize: true,
        //filterable: true,
        selectionmode: 'singlerow',
        columns: [
            { text: 'ФИО', datafield: "ФИО", width: 200 },
            { text: '№', datafield: "Код", width: 60, hidden: true },
            { text: 'С', datafield: "Статус", width: 10, hidden: true },
            { text: 'Долг', datafield: "IsZadolzh", width: 40, columntype: 'checkbox' }
        ],
        width: '100%',
        height: '97%'
    });
    
    studentGrid.jqxGrid({ altrows: true });
    
    // Подключаем обработчик выделения студентов
    $(studentGrid).bind('rowselect', function (event) {
        studentSelected(event);
    });
}

function studentSelected(event) {
    var selectedRowIndex = event.args.rowindex; 
    // Получем данные студента из текущей строки
    curStudId = studentGrid.jqxGrid('getcell', selectedRowIndex, 'Код').value;
    curStudFIO = studentGrid.jqxGrid('getcell', selectedRowIndex, 'ФИО').value;
    curStudStatus = studentGrid.jqxGrid('getcell', selectedRowIndex, 'Статус').value;
    curIsStudZadolzh = studentGrid.jqxGrid('getcell', selectedRowIndex, 'IsZadolzh').value;

    var strZadolzh;
    if (curIsStudZadolzh == true) {
        strZadolzh = 'Должник';
    }        
    else {
        strZadolzh = 'Нет долгов';
    }

    $("#curStudId").text(curStudId);
    $("#curStudFIO").text(curStudFIO);
    $("#curStudStatus").text(curStudStatus);
    $("#curIsStudZadolzh").text(strZadolzh);

    InitZadolzhGrid_DataBind(curStudId);
}

/* Инициализация интерфейса Grid с бегунками */
function zadolzhGridInit_Interface() {
    begunGrid.jqxGrid({
        //source: dataAdapter,
        //sortable: true,
        columnsresize: true,
        selectionmode: 'singlerow',
        columns: [
            { text: 'Дисциплина', datafield: "Дисциплина", width: 300 },
            { text: 'Курс', datafield: "Курс", width: 40 },
            { text: 'Семестр', datafield: "СеместрВыч", width: 40 },
            { text: '№Вед.', datafield: "вед_Код", width: 80 },
            { text: 'Описание', datafield: "Описание", width: 50 }, 
            { text: 'Оц(вед)', datafield: "Итог", width: 70 },
            { text: 'Преподаватель', datafield: "вед_Преподаватель", width: 150 },
            { text: '№Бег.', datafield: "id", width: 60 },
            { text: 'Оц(бег)', datafield: "ocenka", width: 60 },
            { text: 'Закр', datafield: "isclosed", width: 50, cellsrenderer: renderDostupBegun },
            { text: 'Семестр', datafield: "Семестр", width: 50, hidden: true }, // hidden
            { text: 'Сессия', datafield: "Сессия", width: 50, hidden: true }, // hidden
            { text: 'ТипВедомости', datafield: "ТипВедомости", hidden: true }, // hidden 
            { text: 'Итоговая_Оценка', datafield: "Итоговая_Оценка", hidden: true } // hidden 
        ],
        width: '100%',
        height: '97%'
    });
    

    // Подключаем обработчик выделения задолженности
    $(begunGrid).bind('rowselect', function (event) {
        begunForStudentSelected(event);
    });    

}

function begunForStudentSelected(event) {
    var selectedRowIndex = event.args.rowindex;
    
    // Получем данные бегунка из текущей строки
    curBegunCode = begunGrid.jqxGrid('getcell', selectedRowIndex, 'id').value;
    curBegunVedCode = begunGrid.jqxGrid('getcell', selectedRowIndex, 'вед_Код').value;

    if (curBegunCode != null) {
        btnEditSelBegun.jqxButton({ disabled: false });
        btnPrintSelBegun.jqxButton({ disabled: false });        
    } else {
        btnEditSelBegun.jqxButton({ disabled: true });
        btnPrintSelBegun.jqxButton({ disabled: true });
    }
}

function renderDostupBegun(row, column, value, defaultHtml, columnSettings, rowData) {
    if (value == false)
        return '<img src="/Content/Images/active_unlock.png" width="30px">';
    else return '<img src="/Content/Images/active_lock.png" width="30px">';
}

/* Загрузка данных для Grid студентов */
function InitStudentGrid_DataBind(pGroupCode)
{
    // Работаем с источником
    var localSource = sourceUrlStudents.replace("param1", pGroupCode);
    //alert(localSource);

    var source =
    {
        datatype: "json",
        type: "POST",
        datafields: [
            { name: 'ФИО', type: 'string' },
            { name: 'Код', type: 'int' },
            { name: 'Статус', type: 'int' },
            { name: 'IsZadolzh', type: 'bool' }
        ],
        url: localSource
    };

    var dataAdapter = new $.jqx.dataAdapter(source,
    {
        loadComplete: function () {
            loadCompleteStudents(studentGrid, dataAdapter);
        }
    });

    // Соединяем источник с таблицей
    $("#studentGrid").jqxGrid({ source: dataAdapter });
}

function InitZadolzhGrid_DataBind(studentId) {
    // Работаем с источником
    var localSource = sourceUrlZadolzh.replace("param1", studentId);
    //alert(localSource);

    var source =
    {
        datatype: "json",
        type: "POST",
        datafields: [
            { name: 'Дисциплина', type: 'string' },
            { name: 'Курс', type: 'int' },
            { name: 'Семестр', type: 'int' },
            { name: 'СеместрВыч', type: 'int' },
            { name: 'Сессия', type: 'int' },
            { name: 'СеместрВыч', type: 'int' },
            { name: 'вед_Преподаватель', type: 'string' },
            { name: 'вед_Код', type: 'int' },
            { name: 'ТипВедомости', type: 'int' },
            { name: 'Описание', type: 'string' },
            { name: 'Итог', type: 'int' },
            { name: 'Итоговая_Оценка', type: 'int' },
            { name: 'id', type: 'int' },
            { name: 'ocenka', type: 'int' },
            { name: 'isclosed', type: 'bool' },
            { name: 'Примечание', type: 'string' }
        ],
        url: localSource
    };

    var dataAdapter = new $.jqx.dataAdapter(source,
    {
        loadComplete: function () {
            loadCompleteBegun(begunGrid, dataAdapter);
        }
    });

    // Соединяем источник с таблицей
    $("#begunGrid").jqxGrid({ source: dataAdapter });
}

function loadCompleteStudents(studentGrid, dataAdapter) {
    // выделяем строку
    var records = dataAdapter.records;
    var rowcount = records.length;
    if (rowcount > 0) {
        studentGrid.jqxGrid('clearselection');
        studentGrid.jqxGrid('selectrow', 0);
    }
}

function loadCompleteBegun(begunGrid, dataAdapter) {
    // выделяем строку    
    var records = dataAdapter.records;
    var rowcount = records.length;
    if (rowcount > 0) {
        begunGrid.jqxGrid('clearselection');
        begunGrid.jqxGrid('selectrow', 0);
    } else {
        btnEditSelBegun.jqxButton({ disabled: true });
        btnPrintSelBegun.jqxButton({ disabled: true });
    }
}

function InitVedomGrid_DataBind(vedomGrid, pGrCode, pPlName) {
    // было:
    //sourceUrlVedom = tmplVedomSourse_1 + pGrCode + tmplVedomSourse_2 + encodeURIComponent(pPlName);

    //стало:
    sourceUrlVedom = sourceUrlVedomCollection.replace("param1", pGrCode);
    sourceUrlVedom = sourceUrlVedom.replace("param2", encodeURIComponent(pPlName));
    sourceUrlVedom = sourceUrlVedom.replace("amp;", "");

    //alert(sourceUrlVedom);
    // Работаем с источником
    var source =
    {
        datatype: "json",
        type: "POST",
        datafields: [
            { name: 'Дисциплина', type: 'string' },
            { name: 'Описание', type: 'string' },
            { name: 'Курс', type: 'int' },
            { name: 'Семестр', type: 'int' },
            { name: 'вед_Преподаватель', type: 'string' },
            { name: 'вед_Код', type: 'int' },
            { name: 'ИмяФайла', type: 'string' },
            { name: 'ТипВедомости', type: 'int' },
            { name: 'ВидСессии', type: 'int' }, 
            { name: 'СеместрВыч', type: 'int' },
            { name: 'IsCurrentAtt', type: 'bool' },
            { name: 'IsNeedReiting', type: 'bool' },
            { name: 'Закрыта', type: 'bool' }
        ],
        url: sourceUrlVedom
    };

    var dataAdapter = new $.jqx.dataAdapter(source,
        {
            loadComplete: function() {
                loadCompleteVedoms(vedomGrid, dataAdapter);
            }
        });
    
    // Соединяем источник с таблицей
    $(vedomGrid).jqxGrid({ source: dataAdapter });

}

function loadCompleteVedoms(vedomGrid, dataAdapter) {
    //alert('vedoms loadd');

    // выделяем строку
    var records = dataAdapter.records;
    var rowcount = records.length;
    if (rowcount > 0) {
        curVedCode = 0;
        $(vedomGrid).jqxGrid('selectrow', curVedCode);
    }
        
}

function vedomSelected(event) {
    var selectedRowIndex = event.args.rowindex;

    // Получем данные из текущей строки
    curVedCode = $(vedomGrid).jqxGrid('getcell', selectedRowIndex, 'вед_Код').value;
    curVedTypeCode = $(vedomGrid).jqxGrid('getcell', selectedRowIndex, 'ТипВедомости').value;
    
    // Устанавливаем надписи
    labelDiscipl.text($(vedomGrid).jqxGrid('getcell', selectedRowIndex, 'Дисциплина').value);
    labelKursVedom.text($(vedomGrid).jqxGrid('getcell', selectedRowIndex, 'Курс').value);
    labelTypeVedom.text($(vedomGrid).jqxGrid('getcell', selectedRowIndex, 'Описание').value);
    labelSess.text($(vedomGrid).jqxGrid('getcell', selectedRowIndex, 'ВидСессии').value);
    labelSemVedom.text($(vedomGrid).jqxGrid('getcell', selectedRowIndex, 'Семестр').value);
    labelSemCalc.text($(vedomGrid).jqxGrid('getcell', selectedRowIndex, 'СеместрВыч').value);
    if (curVedCode == null)
        labelVedomNo.text('-');
    else
        labelVedomNo.text(curVedCode);

    if (curVedCode != null) {
        // Если ведомость уже существует lnkEditVedom
        $(btnNewVedom).jqxButton({ disabled: true });
        
        $(lnkEditVedom).jqxLinkButton({ disabled: false });
        $("a#lnkEditVedom").jqxLinkButton("href", sourceUrlVedomEdit.replace("param1", curVedCode));
        $(lnkPrintEmpty).jqxLinkButton({ disabled: false });
        
        $(lnkPrintBlank).jqxLinkButton({ disabled: false });
        var curRef = sourceUrlVedomPrint.replace("amp;", "");
        curRef = curRef.replace("param1", curVedCode);
        curRef = curRef.replace("param2", 0);
        lnkPrintBlank.jqxLinkButton("href", curRef);
    }
    else {
        // Если ведомости еще нет
        $(btnNewVedom).jqxButton({ disabled: false });
        $(lnkEditVedom).jqxLinkButton({ disabled: true });
        $(lnkPrintEmpty).jqxLinkButton({ disabled: true });
        $(lnkPrintBlank).jqxLinkButton({ disabled: true });
    }

}

function InitVedomButtonsControl() {
    $(btnNewVedom).on('click',
        function () {
            /* Создаем новую ведомость в базе */
        
            // Устанавливаем параметры
            var locDiscipl = $(labelDiscipl).text();
            var locKurs = $(labelKursVedom).text();
            var locSem = $(labelSemVedom).text();
            var locSess = $(labelSess).text();

            // Выполняем создание
            CreateNewVedom(curGroupCode, curPlanName, locDiscipl, locKurs, locSem, locSess, curVedTypeCode);
        });

    //Экспорт списка ведомостей в Excel
    var btnExport = $("#btnExport");
    btnExport.jqxLinkButton({ width: '60', height: '25' });
    btnExport.on('click',
        function () {
            
        });

}

function CreateNewVedom(groupCode, planName, discipl, kurs, sem, sess, vedType) {
    
    $.ajax({
        type: "GET",
        url: sourceUrlVedomAdd,
        contentType: "application/json; charset=utf-8",
        data: { 'pGroupCode': groupCode, 'pPlanName': planName, 'pDiscipl': discipl, 'pKurs': kurs, 'pSem': sem, 'pSess': sess, 'pVedType': vedType },
        dataType: "json",
        success: function(res) {
            $(vedomGrid).jqxGrid('updatebounddata');
        },
        error: function(res) {
            alert("Ошибка при создании ведомости. Обратитесь к разработчикам.");
        }
    });

}

function InitDlgEditSelBegun() {
    // Создаем окно диалога
    dlgEditSelBegun.jqxWindow({
        height: 150,
        width: 300,
        resizable: false,
        isModal: true,
        autoOpen: false,
        initContent: function () {
            $("#dlgEditSelBegunOcenka").jqxNumberInput({ min: 0, max: 9, decimalDigits: 0, width: '100px', height: '25px', promptChar: '_', digits: 1 });
            $('#okSelBegun').jqxButton({ width: '150px' });
            $('#cancelSelBegun').jqxButton({ width: '85px' });
            $('#dlgEditSelBegunOcenka').focus();
        }
    });

    // Подключаем обработчики
    $('#okSelBegun').on('click', function () { 
        onSaveSelBegun();
        dlgEditSelBegun.jqxWindow('close');
    });

    $('#cancelSelBegun').on('click', function () {
        dlgEditSelBegun.jqxWindow('close');
    });
}

function onSaveSelBegun() {
    var locUrl = sourceUrlSaveBegun;

    var ocen = $("#dlgEditSelBegunOcenka").val();
    //alert(ocen);
    
    // Сохранение
    $.ajax({
        type: "POST",
        url: locUrl,
        //contentType: "application/json; charset=utf-8",
        data: {'pBegunId':curBegunCode, 'pOcenkaCode': ocen},
        dataType: "json",
        success: function (res) {
            $(begunGrid).jqxGrid('updatebounddata');
        },
        error: function (x, e) {
            alert('error');
        }
    });
}