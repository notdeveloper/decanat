﻿

function InitMainSplitter() {
    //alert('from js');
    var splt = $("#mainsplitter");

    splt.jqxSplitter(
    {
        width: '100%',
        height: '97%',
        panels: [{ size: '20%' }, { size: '80%' }]
    });
}

function InitMainTab() {
    var tab = $("#mainTab");

    tab.jqxTabs({ width: '100%', height: '100%', position: 'top' });
}

function InitGroupTree(){
    //groupTreeGrid
    var gr = $("#groupTreeGrid");

    // Init interface of grid
    InitGroupTreeInterface(gr);
    //InitVedomGridInterface(gr);
}

function InitGroupTreeInterface(group_grid) {
    group_grid.jqxGrid({
        width: '100%',
        height: '90%',
        columns: [
            { text: "Группа", width: 100 },
            { text: "Кол-во", width: 100 }
        ]
    });
}

function InitVedomGrid() {
    var vgr = $("#vedomGrid");

    InitVedomGridInterface(vgr);
    //InitVedomGridData(vgr);
}

function InitVedomGridInterface(vedom_grid) {

    vedom_grid.jqxGrid({
        width: '100%',
        height: '90%',
        columns: [
            { text: "Дисциплина", width: 100 },
            { text: "А", width: 50 }, // текущая аттестация
            { text: "Д", width: 50 }, // доступ
            { text: "Р", width: 50 } // рейтинг
        ]
    });
}

function InitVedomGridData(vedom_grid) {

    var source =
    {
        datatype: 'json',
        type: 'GET',
        datafields:
        [
            { name: 'Дисциплина', type: 'string' },
            { name: 'IsCurrentAtt', type: 'bool' },
            { name: 'IsClosed', type: 'bool' },
            { name: 'IsNeedReiting', type: 'bool' }
            //{ name: 'Зачетка', type: 'string' },
            //{ name: 'РейтинговыйБалл', type: 'int' },
            //{ name: 'ДопускПоОплате', type: 'bool' },
            //{ name: 'Итоговая_Оценка', type: 'int' },
            //{ name: 'Итог', type: 'int' },
            //{ name: 'Рейтинг_Оценка', type: 'int' },
            //{ name: 'ИнтИОц', type: 'int' },
            //{ name: 'ИнтИОцО', type: 'string' },
            //{ name: 'ИнтИтогО', type: 'string' },
            //{ name: 'МожетПравить', type: 'bool' }

        ],
        url: ""//urlOcenkiDataSource
    };

    var dataAdapter = new $.jqx.dataAdapter(source,
    {
        loadComplete: function () {
            //loadCompleteOcenki(ocenkiGrid, dataAdapter);
            alert('load');
        }
    });

    // Присоединяем данные к таблице
    vedom_grid.jqxGrid({ source: dataAdapter });
}