﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Antlr.Runtime.Tree;

namespace Decanat.Models
{
    public class VedomDescription
    {
        // Основное содержимое
        public int VedCode { get; private set; }
        public Dictionary<string, string> DescriptionItems { get; private set; }

        // Важные элементы
        public string GroupName { get; private set; }
        public string God { get; private set; }
        public string FOName { get; private set; }
        public string Discipl { get; private set; }
        public string Department { get; private set; }
        public string Kurs { get; private set; }
        public string Semestr { get; private set; }
        public string Sess { get; private set; }

        public string Spec { get; private set; }
        public string VedType { get; private set; }
        public string IsClosed { get; private set; }
        public string IsCurrentAtt { get; private set; }
        public string IsNeedReiting { get; private set; }

        // Часы
        public string ChasTotal { get; private set; }
        public string ChasLab { get; private set; }
        public string ChasPract { get; private set; }
        public string ChasLek { get; private set; }

        // Преподаватели
        public string FIOKP { get; private set; }
        public string FIOKR { get; private set; }
        public string FIOLab { get; private set; }
        public string FIOLektor { get; private set; }
        public string FIOPrakt { get; private set; }

        public VedomDescription(int pVedCode, bool NeedDictionary)
        {
            VedCode = pVedCode;
            var context = new JournalEntities();
            var dbResult = context.GetVedomDescription(pVedCode);

            DescriptionItems = dbResult.ToDictionary(t => t.TKey, t => t.TValue);

            // Устанавливаем важные эелементы
            GroupName = DescriptionItems["[Group]"];
            God = DescriptionItems["[God]"];
            FOName = DescriptionItems["[FOName]"];
            Discipl = DescriptionItems["[Discipl]"];
            Department = DescriptionItems["[Fak]"];
            Kurs = DescriptionItems["[Kurs]"];
            Sess = DescriptionItems["[Sess]"];

            // Вычисляем семестр
            if (FOName == "ОФ")
                Semestr = DescriptionItems["[Sem]"];
            else
            {
                if (Sess == "1")
                    Semestr = (Convert.ToInt32(Kurs) * 2 - 1).ToString();
                else
                    Semestr = (Convert.ToInt32(Kurs) * 2).ToString();
            }

            Spec = DescriptionItems["[Spec]"];
            VedType = DescriptionItems["[VedType]"];
            ChasTotal = DescriptionItems["[ChasTotal]"];
            ChasLab = DescriptionItems["[ChasLab]"];
            ChasPract = DescriptionItems["[ChasPrakt]"];
            ChasLek = DescriptionItems["[ChasLek]"];
            FIOKP = DescriptionItems["[FIOKP]"];
            FIOKR = DescriptionItems["[FIOKR]"];
            FIOLab = DescriptionItems["[FIOLab]"];
            FIOLektor = DescriptionItems["[FIOLektor]"];
            FIOPrakt = DescriptionItems["[FIOPrakt]"];
            IsClosed = DescriptionItems["[IsClosed]"];
            IsCurrentAtt = DescriptionItems["[IsCurrentAtt]"];
            IsNeedReiting = DescriptionItems["[IsNeedReiting]"];

            if (!NeedDictionary)
                DescriptionItems.Clear();
        }
    }
}