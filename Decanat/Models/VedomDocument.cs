﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Decanat.Models
{
    public class VedomDocument
    {
        // Основное содержимое
        public int VedCode { get; private set; }
        public Dictionary<string, string> DescriptionItems { get; private set; }
        public List<OcenkiCollectionItem> OcenkiItems { get; private set; }

        // Важные элементы
        public string GroupName { get; private set; }
        public string God { get; private set; }
        public string FOName { get; private set; }
        public string Discipl { get; private set; }
        public string Department { get; private set; }
        public string Kurs { get; private set; }
        public string Semestr { get; private set; }
        public string Spec { get; private set; }
        public string VedType { get; private set; }
        public string VedTypeKode { get; private set; }
        public string Sess { get; private set; }
        // Часы
        public string ChasTotal { get; private set; }
        public string ChasLab { get; private set; }
        public string ChasPract { get; private set; }
        public string ChasLek { get; private set; }
        public string ChasTotalDisc { get; private set; }
        // Преподаватели
        public string FIOKP { get; private set; }
        public string FIOKR { get; private set; }
        public string FIOLab { get; private set; }
        public string FIOLektor { get; private set; }
        public string FIOPrakt { get; private set; }
        public string FIOPrinimal { get; private set; } // Кто принимал текущий вид работ

        public VedomDocument(int pVedCode, bool NeedDictionary)
        {
            VedCode = pVedCode;
            var context = new JournalEntities();

            // Загружаем описание (шапку ведомости)
            var dbResultDescription = context.GetVedomDescription(pVedCode);

            // Загружаем коллекцию оценок для ведомости
            var dbResultOcenki = context.GetOcenkiCollectionForVedom(pVedCode, 1);

            DescriptionItems = dbResultDescription.ToDictionary(t => t.TKey, t => t.TValue);

            /*
    
            oc.оц_Код,
            oc.вс_Код,
            УчетВВед=oc.УчетВВед,
            ФИО=oc.Фамилия + " " + oc.Имя + " " + oc.Отчество,
            Зачетка=oc.Зачетка,
            oc.РейтинговыйБалл,
            oc.ДопускПоОплате,
            oc.Итоговая_Оценка,
            oc.Итог,
            oc.Рейтинг_Оценка,
            oc.ИнтИОц,
            oc.ИнтИОцО,
            oc.ИнтИтогО,
            oc.МожетПравить    

            */

            OcenkiItems = (from c in dbResultOcenki
                           select
                          new OcenkiCollectionItem
                          {
                              оц_Код = c.оц_Код,
                              вс_Код = c.вс_Код,
                              УчетВВед = c.УчетВВед,
                              Фамилия = c.Фамилия,
                              Имя = c.Имя,
                              Отчество = c.Отчество,
                              Зачетка = c.Зачетка,
                              РейтинговыйБалл = c.РейтинговыйБалл,
                              ДопускПоОплате = c.ДопускПоОплате,
                              Итоговая_Оценка = c.Итоговая_Оценка,
                              Итог = c.Итог,
                              Рейтинг_Оценка = c.Рейтинг_Оценка,
                              ИнтИОц = c.ИнтИОц,
                              ИнтИОцО = c.ИнтИОцО,
                              ИнтИтогО = c.ИнтИтогО,
                              МожетПравить = c.МожетПравить,
                              вед_Код = c.вед_Код
                          }).ToList(); 


            // Устанавливаем важные эелементы
            GroupName = DescriptionItems["[Group]"];
            God = DescriptionItems["[God]"];
            FOName = DescriptionItems["[FOName]"];
            Discipl = DescriptionItems["[Discipl]"];
            Department = DescriptionItems["[Fak]"];
            Kurs = DescriptionItems["[Kurs]"];
            Sess = DescriptionItems["[Sess]"];

            // Вычисляем семестр
            if (FOName == "ОФ")
                Semestr = DescriptionItems["[Sem]"];
            else
            {
                if (Sess == "1")
                    Semestr = (Convert.ToInt32(Kurs) * 2 - 1).ToString();
                else
                    Semestr = (Convert.ToInt32(Kurs) * 2).ToString();
            }

            Spec = DescriptionItems["[Spec]"];
            VedType = DescriptionItems["[VedType]"];
            VedTypeKode = DescriptionItems["[VedTypeKod]"];

            ChasTotal = DescriptionItems["[ChasTotal]"];
            ChasLab = DescriptionItems["[ChasLab]"];
            ChasPract = DescriptionItems["[ChasPrakt]"];
            ChasLek = DescriptionItems["[ChasLek]"];
            ChasTotalDisc = DescriptionItems["[ChasTotalDisc]"];

            FIOKP = DescriptionItems["[FIOKP]"];
            FIOKR = DescriptionItems["[FIOKR]"];
            FIOLab = DescriptionItems["[FIOLab]"];
            FIOLektor = DescriptionItems["[FIOLektor]"];
            FIOPrakt = DescriptionItems["[FIOPrakt]"];

            // Находим ФИО принимающего
            FIOPrinimal = DescriptionItems["[FIOPrinimal]"];
        }
    }
}