﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Objects;

namespace Decanat.Models
{
    public class ImportOcenki
    {
        public int RowsProcessed { get; private set; }
        public String ResMessage { get; private set; }

        public ImportOcenki(int pVedCode, int pVedTypeCode, int pKurs, int pSem, string pSource)
        {
            var context = new JournalEntities();

            ObjectParameter opRowsProcessed = new ObjectParameter("rows_processed", typeof(int));
            ObjectParameter opResMessage = new ObjectParameter("res_message", typeof(String));

            var dbResult = context.Import(pVedCode, pVedTypeCode, pKurs, pSem, pSource, "test_import", 
                opRowsProcessed, opResMessage);

            // Force execution
            //this.OcenkiCollection = dbResult.ToList();

            RowsProcessed = Convert.ToInt32(opRowsProcessed.Value);
            ResMessage = Convert.ToString(opResMessage.Value);
        }
    }
}