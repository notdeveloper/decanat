﻿using System.Web;
using System.Web.Mvc;
using Decanat.Filters;

namespace Decanat
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new DecanatAuthorize());
        }
    }
}